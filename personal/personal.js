//import "../vendors/@progress/kendo-ui/2020.3.1118/js/kendo.all.min.js"

$(() => {
	$('#grid').kendoGrid({
		height: 550,
		sortable: true
	});

	$('#name').kendoAutoComplete(["kongsak", "saluda"]);
}
);


$(function() {
	$("#scheduler").kendoScheduler({
		date: new Date("2021/2/2"),
		startTime: new Date("2021/2/2 07:00 AM"),
		height: 300,
		//width: 400,
		views: [
        //"day",
        { type: "month", selected: true },
        "month",
        "agenda",
        //{ type: "timeline", eventHeight: 50}
        ],
        timezone: "Etc/UTC",
        dataSource: {
        	batch: true,
        	transport: {
        		read: {
        			url: "https://demos.telerik.com/kendo-ui/service/tasks",
        			dataType: "jsonp"
        		},
        		update: {
        			url: "https://demos.telerik.com/kendo-ui/service/tasks/update",
        			dataType: "jsonp"
        		},
        		create: {
        			url: "https://demos.telerik.com/kendo-ui/service/tasks/create",
        			dataType: "jsonp"
        		},
        		destroy: {
        			url: "https://demos.telerik.com/kendo-ui/service/tasks/destroy",
        			dataType: "jsonp"
        		},
        		parameterMap: function(options, operation) {
        			if (operation !== "read" && options.models) {
        				return {models: kendo.stringify(options.models)};
        			}
        		}
        	},
        	schema: {
        		model: {
        			id: "taskId",
        			fields: {
        				taskId: { from: "TaskID", type: "number" },
        				title: { from: "Title", defaultValue: "No title", validation: { required: true } },
        				start: { type: "date", from: "Start" },
        				end: { type: "date", from: "End" },
        				startTimezone: { from: "StartTimezone" },
        				endTimezone: { from: "EndTimezone" },
        				description: { from: "Description" },
        				recurrenceId: { from: "RecurrenceID" },
        				recurrenceRule: { from: "RecurrenceRule" },
        				recurrenceException: { from: "RecurrenceException" },
        				ownerId: { from: "OwnerID", defaultValue: 1 },
        				isAllDay: { type: "boolean", from: "IsAllDay" }
        			}
        		}
        	},
        	filter: {
        		logic: "or",
        		filters: [
        		{ field: "ownerId", operator: "eq", value: 1 },
        		//{ field: "ownerId", operator: "eq", value: 2 },
        		//{ field: "ownerId", operator: "eq", value: 3 }
        		]
        	}
        },
        resources: [
        {
        	field: "ownerId",
        	title: "Owner",
        	dataSource: [
        	{ text: "Alex", value: 1, color: "#2eb85c" },
        	{ text: "Bob", value: 2, color: "#2eb85c" },
        	{ text: "Charlie", value: 3, color: "#2eb85c" }
        	]
        }
        ]
    });

	$("#people :checkbox").change(function(e) {
		var checked = $.map($("#people :checked"), function(checkbox) {
			return parseInt($(checkbox).val());
		});

		var scheduler = $("#scheduler").data("kendoScheduler");

		scheduler.dataSource.filter({
			operator: function(task) {
				return $.inArray(task.ownerId, checked) >= 0;
			}
		});
	});
});

var wnd,detailsTemplate;
$(document).ready(function () {
	var crudServiceBaseUrl = "https://demos.telerik.com/kendo-ui/service",
	dataSource = new kendo.data.DataSource({
		transport: {
				create: {
				//url: crudServiceBaseUrl + "/detailproducts/Create",
				//cache: true,
				//	dataType: "jsonp" // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
				},
				read: {
                    //url: "http://localhost:9999/personal/datatable.json",
                    url: "company_data.json",//crudServiceBaseUrl + "/detailproducts""",
                    dataType: "json"
                },
                update: {
                	//url: crudServiceBaseUrl + "/detailproducts/Update",
                	//dataType: "jsonp"
                },
                destroy: {
                	//url: crudServiceBaseUrl + "/detailproducts/Destroy",
                	//dataType: "jsonp"
                },
                parameterMap: function (options, operation) {
                	if (operation !== "read" && options.models) {
                		return { models: kendo.stringify(options.models) };
                	}
                }
            },
            batch: true,
            pageSize: 10,
            autoSync: true,
            schema: {
            	model: {
            		id: "CompanyCode",
            		fields: {
            			CompanyCode: { editable: false, nullable: true },
            			CompanyName: { editable: true },
            			Status: { editable: true },
            		}
            	}
            }
        });

	$("#companyGrid").kendoGrid({
		dataSource: dataSource,
		columnMenu: {
			filterable: false
		},
		height: 600,
		pageable: true,
		sortable: true,
		navigatable: true,
		resizable: false,
		reorderable: false,
		groupable: false,
		filterable: true,

		dataBound: onDataBound,
		toolbar: ["create","search"],
		columns: [
            /*{
                selectable: true,
                width: 25,
                attributes: {
                    "class": "checkbox-align",
                },
                headerAttributes: {
                    "class": "checkbox-align",
                }
            },*/ {
            	field: "CompanyCode",
            	title: "Company Code",
            	width: 200
            }, {
            	field: "CompanyName",
            	title: "Company Name",
            	format: "{0:c}",
            	width: 200
            }, {
            	field: "Status",
            	title: "Status",
            	template: "<span id='badge_#=CompanyCode#' class='badgeTemplate'></span>",
                //template: "<a role='button'  data-toggle='modal' class='k-button k-button-icontext k-grid-edit' href='#EditCompany'><span class='k-icon k-i-edit'></span>open</a>",
                width: 100,
            },

            { command: ["edit", "destroy"], title: "&nbsp;", width: "300px" },
            //{ command: "destroy", title: "&nbsp;", width: 100 },
            ],editable: "popup"
        });




});
function customBoolEditor(container, options) {
  //  $('<input class="k-checkbox" type="checkbox" name="Discontinued" data-type="boolean" data-bind="checked:Discontinued">').appendTo(container);
}
function onDataBound(e) {
	var grid = this;

	grid.table.find("tr").each(function () {
		var dataItem = grid.dataItem(this);
		var themeColor = dataItem.Status ? 'success' : 'error';
		//var text = dataItem.Status ? 'available' : 'not available';

		$(this).find(".badgeTemplate").kendoBadge({
			themeColor: themeColor,
			text: text,
		});

		$(this).find(".rating").kendoRating({
			min: 1,
			max: 5,
			label: false,
			selection: "continuous"
		});



		kendo.bind($(this), dataItem);
	});
}

function returnFalse() {
	return false;
}


function createChartOverall() {
	$("#chartOverall").kendoChart({
		title: {
			text: "Overall Performance"
		},
		legend: {
			position: "bottom"
		},
		chartArea: {
			background: "",
			height: 240
		},
		seriesDefaults: {
			type: "line",
			style: "smooth"
		},
		series: [{
			name: "India",
			data: [3.907, 7.943, 7.848, 9.284]
		},{
			name: "World",
			data: [1.988, 2.733, 3.994, 3.464]
		},{
			name: "Russian Federation",
			data: [4.743, 7.295, 7.175, 6.376]
		},{
			name: "Haiti",
			data: [-0.253, 0.362, -3.519, 1.799]
		}],
		valueAxis: {
			labels: {
				format: "{0}%"
			},
			line: {
				visible: false
			},
			axisCrossingValue: -10
		},
		categoryAxis: {
			categories: [2002, 2003, 2004, 2005],
			majorGridLines: {
				visible: false
			},
			labels: {
				rotation: "auto"
			}
		},
		tooltip: {
			visible: true,
			format: "{0}%",
			template: "#= series.name #: #= value #"
		}
	});
}
function createChartMind() {
	$("#chartMindset").kendoChart({
		title: {
			text: "Overall Mindset"
		},
		legend: {
			position: "top"
		},
		chartArea: {
			background: "",
			height: 240
		},
		seriesDefaults: {
			type: "line",
			style: "smooth"
		},
		series: [{
			name: "India",
			data: [3.907, 7.943, 7.848, 9.284]
		},{
			name: "World",
			data: [1.988, 2.733, 3.994, 3.464]
		},],
		valueAxis: {
			labels: {
				format: "{0}%"
			},
			line: {
				visible: false
			},
			axisCrossingValue: -10
		},
		categoryAxis: {
			categories: [2002, 2003, 2004, 2005],
			majorGridLines: {
				visible: false
			},
			labels: {
				rotation: "auto"
			}
		},
		tooltip: {
			visible: true,
			format: "{0}%",
			template: "#= series.name #: #= value #"
		}
	});
}

$(document).ready(createChartOverall);
$(document).ready(createChartMind);

//$(document).bind("kendo:skinChange", createChartMindset);
$(document).bind("kendo:skinChange", createChartOverall);
$(document).bind("kendo:skinChange", createChartMind);
